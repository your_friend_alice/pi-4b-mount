$fn=100;
HOLE_SPACING=[58, 49];
BOARD_SIZE=[85, 56];
BOARD_WIGGLE_ROOM=0.2;
CARRIER_SIZE=[110, 58];
CARRIER_SCREW_EDGE_DISTANCE=8;
CARRIER_SCREW_DIAMETER=4;
HOLE_SIZE=2.5;
POST_SIZE=7;
BOARD_THICKNESS=1.4;
NUB_HEIGHT=2;
BOTTOM_CLEARANCE=3;
BOTTOM_PLATE_THICKNESS=1;
INNER_RING_WIDTH=1;
BOARD_CORNER_RADIUS=3;

module cornerPosts() {
    for(x=[-1,1]) for(y=[-1,1]){
        translate([x*HOLE_SPACING[0]/2, y*HOLE_SPACING[1]/2, 0]) {
            translate([0,0,(BOARD_THICKNESS+NUB_HEIGHT)/2+BOTTOM_CLEARANCE]) {
                cylinder(d=HOLE_SIZE, h=BOARD_THICKNESS+NUB_HEIGHT, center=true);
            }
            translate([0,0,BOTTOM_CLEARANCE/2]) {
                cylinder(d=POST_SIZE, h=BOTTOM_CLEARANCE, center=true);
                translate([-POST_SIZE/4*x, POST_SIZE/4*y, 0]) cube([POST_SIZE/2, POST_SIZE/2, BOTTOM_CLEARANCE], center=true);
                translate([POST_SIZE/4*x, POST_SIZE/4*y, 0]) cube([POST_SIZE/2, POST_SIZE/2, BOTTOM_CLEARANCE], center=true);
                if(x > 0) {
                    translate([POST_SIZE/4*x, -POST_SIZE/4*y, 0]) cube([POST_SIZE/2, POST_SIZE/2, BOTTOM_CLEARANCE], center=true);
                }
            }
        }
    }
}

module roundedRect(size, r) {
    hull() for(x=[-1,1]) for(y=[-1,1]) {
        translate([x*(size[0]/2-r), y*(size[1]/2-r), 0]) cylinder(r=r, h=size[2], center=true);
    }
}

module carrier() {
    height = BOARD_THICKNESS+BOTTOM_CLEARANCE+BOTTOM_PLATE_THICKNESS;
    translate([0,0,height/2]) {
        difference() {
            roundedRect([CARRIER_SIZE[0], CARRIER_SIZE[1], height], r=3);
            roundedRect([BOARD_SIZE[0], BOARD_SIZE[1], height+1], r=BOARD_CORNER_RADIUS);
            for(x=[-1,1]) for(y=[-1,1]) {
                translate([x*(CARRIER_SIZE[0]-CARRIER_SCREW_EDGE_DISTANCE)/2, y*(CARRIER_SIZE[1]-CARRIER_SCREW_EDGE_DISTANCE)/2, 0]) cylinder(d=CARRIER_SCREW_DIAMETER, h=height+1, center=true);
            }
        }
    }
}

difference() {
    union() {
        intersection() {
            roundedRect([BOARD_SIZE[0], BOARD_SIZE[1], (BOTTOM_PLATE_THICKNESS+BOARD_THICKNESS+BOTTOM_CLEARANCE+NUB_HEIGHT)*2], r=BOARD_CORNER_RADIUS);
            union() {
                // bottom plate
                translate([0,0,BOTTOM_PLATE_THICKNESS/2]) cube([BOARD_SIZE[0], BOARD_SIZE[1], BOTTOM_PLATE_THICKNESS], center=true);
                // corner posts
                translate([(BOARD_SIZE[0]-HOLE_SPACING[0]-POST_SIZE)/2,0,BOTTOM_PLATE_THICKNESS]) cornerPosts();
                // support ring under the board
                translate([0,0,BOTTOM_PLATE_THICKNESS+BOTTOM_CLEARANCE/2]) {
                    difference() {
                        cube([BOARD_SIZE[0]+BOARD_WIGGLE_ROOM*2, BOARD_SIZE[1]+BOARD_WIGGLE_ROOM*2, BOTTOM_CLEARANCE], center=true);
                        roundedRect([BOARD_SIZE[0]-2*INNER_RING_WIDTH, BOARD_SIZE[1]-2*INNER_RING_WIDTH, BOTTOM_CLEARANCE+1], r=BOARD_CORNER_RADIUS-INNER_RING_WIDTH);
                    }
                }
            }
        }
        carrier();
    }
    translate([BOARD_SIZE[0]/2-INNER_RING_WIDTH,0,BOTTOM_PLATE_THICKNESS+BOTTOM_CLEARANCE/2]) cube([INNER_RING_WIDTH*2+10, 20, BOTTOM_CLEARANCE], center=true);
}
